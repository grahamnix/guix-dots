(use-modules (gnu))
(use-service-modules networking ssh)
(use-package-modules ssh emacs version-control xorg gtk fonts code)

(operating-system
  (host-name "guix-qemu")
  (timezone "Canada/Pacific")
  (locale "en_US.utf8")

  (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
                (target "/dev/sda")))
  (file-systems (cons (file-system
                        (device (file-system-label "guix"))
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))

  (users (cons (user-account
                (name "gbouvier")
                (comment "Graham Bouvier")
                (group "users")

                (supplementary-groups '("wheel"))
                (home-directory "/home/gbouvier"))
               %base-user-accounts))

  (packages (cons*
	     openssh
	     emacs
	     git
	     the-silver-searcher
	     xauth
	     font-iosevka
	     %base-packages))

  (services (cons* (service openssh-service-type
                            (openssh-configuration
			      (x11-forwarding? #t)))
                   %base-services)))
